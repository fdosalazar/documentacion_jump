# Documentación lógica custom page Jump

Las tecnologias usadas para crear las custom page fueron:
> Bootstrap: Funcionamiento responsive y HTML5 [Documentación Bootstrap](http://getbootstrap.com/components/)
> jQuery: Logica texto y manejo del DOM [Documentación jQuery](http://api.jquery.com/)
> CSS3: Estilos personalizados [Documentación CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)

Los CSS son totalmente customizables para resolver lo que necesiten.
No olvidar que estos tienen que ir en el mismo documento html a menos que tengan un servidor y los lean desde ahí.

### Mensajes y textos en otro idioma

##### Codigo:
```javascript
$.noConflict();
jQuery( document ).ready(function( $ ) {
    var rawText = $('.raw-message').html();
    var browserLang = navigator.language || navigator.userLanguage;
    var neddle = 'enter your login information below';

    if(browserLang.toLowerCase().indexOf('es') > -1){
        $('.normal-text-spanish').removeClass('hide');
        $('.title-spanish').removeClass('hide');

        if(rawText.toLowerCase().indexOf(neddle) == -1){
            $('.warning-text-spanish').removeClass('hide');
        }
    } else {
        $('.normal-text-english').removeClass('hide');
        $('.title-english').removeClass('hide');
        
        $('.enable-app span').html('Activate Mobile App Using QR Code');

        $('input[name=username]').attr('placeholder', 'Username');
        $('input[name=password]').attr('placeholder', 'Password');
        
        if(rawText.toLowerCase().indexOf(neddle) == -1){
            $('.warning-text-english').removeClass('hide');
        }
    }
    
    $('.loginFooterMsg').parents('body div').addClass('hide');
});
```
##### Explicacion

Evita el conflicto con otras librerias que utilizan el signo __$__
```javascript 
$.noConflict();
```

Encapsula el codigo en un objeto jQuery y además se pasa como parametro el signo __$__ para ser usado dentro de ese bloque.
El evento __( document ).ready__  significa que todo lo que esté en ese bloque se ejecutará cuando el __document__ (docuemnto) este listo
esto significa que se se ejecutara cuando el browser lea el archivo html, pero las fuentes externas como javascript, css o imagenes adicionales aun no se renderice.
```javascript
jQuery( document ).ready(function( $ ) {
```

```javascript
var rawText = $('.raw-message').html();
var browserLang = navigator.language || navigator.userLanguage;
var neddle = 'enter your login information below';
```
Declaracion y asignacion de variables a utilizar
__rawText__ obtiene el html que se cargue en el elemento que contenga la clase __raw-message__ que en este caso corresponde a una etiqueta __p__
en donde se carga el tag __${WARNING_MSG}__ que contiene el mensaje de "error" o informacion.

```html
<p class='raw-message hide'>${WARNING_MSG}</p>
```
__browserLang__ obtiene el idioma en el cual se encuentra el navegador, la cual se utiliza para mostrar u ocultar los mensajes en los distintos idiomas.
__needle__ texto que se utilizará para mostrar u ocultar el contenido dependiendo del idioma en el que lo entrega SFSF

```javascript
if(browserLang.toLowerCase().indexOf('es') > -1){
    $('.normal-text-spanish').removeClass('hide');
    $('.title-spanish').removeClass('hide');

    if(rawText.toLowerCase().indexOf(neddle) == -1){
        $('.warning-text-spanish').removeClass('hide');
    }
} else {
    $('.normal-text-english').removeClass('hide');
    $('.title-english').removeClass('hide');
    
    $('.enable-app span').html('Activate Mobile App Using QR Code');

    $('input[name=username]').attr('placeholder', 'Username');
    $('input[name=password]').attr('placeholder', 'Password');
    
    if(rawText.toLowerCase().indexOf(neddle) == -1){
        $('.warning-text-english').removeClass('hide');
    }
}
```

Se toma la variable __browserLang__ y se traspasa a lowercarse con la instruccion __toLowerCase()__ y se pregunta si el texto __es__ se encuentra dentro de la variable __browserLang__ con el metodo __indexOf('es')__, lo que esto devuelve es el lugar de la primera coincidencia (ejemplo: [Index Of en w3school](http://www.w3schools.com/jsref/jsref_indexof.asp) ) si no se encuentra en la cadena de texto esto devuelve -1.
Si lo contiene se elimina la clase __hide__ del elemento con clase __normal-text-spanish__ y __title-spanish__ en caso contrario se muestran los textos en ingles. Luego se evalua si el texto que se muestra es el error para quitarle la clase __hide__, esto tiene la misma logica que el paso anterior.

```javascript
$('.loginFooterMsg').parents('body div').addClass('hide');
```
Oculta el texto copyright que añade SFSF en la instancia.


